package br.com.senac.calculadoracomdigito;






import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String ZERO = "0";
    private static final String PONTO = ".";
    private TextView textViewDisplay;
    private Button btnSoma;
    private Button btnSubtracao;
    private Button btnMultiplicacao;
    private Button btnDivisao;
    private Button btnIgual;
    private Button btnQuatro;
    private Button btnCinco;
    private Button btnSeis;
    private Button btnTres;
    private Button btnDois;
    private Button btnUm;
    private Button btnNove;
    private Button btnOito;
    private Button btnSete;
    private Button btnPonto;
    private Button btnZero;
    private Button btnCe;
    private TextView display;
    private double operando1;
    private double operando2;
    private boolean limpar;

    private final int SOMA = 1;
    private final int SUBTRACAO = 2;
    private final int MULTIPLICACAO = 3;
    private final int DIVISAO = 4;
    private final int IGUAL = 5;

    private void initComponentes() {
        textViewDisplay = (TextView) findViewById(R.id.display);

        btnNove = (Button) findViewById(R.id.btnNove);
        btnOito= (Button) findViewById(R.id.btnOito);
        btnSete = (Button) findViewById(R.id.btnSete);
        btnQuatro = (Button) findViewById(R.id.btnQuatro);
        btnCinco = (Button) findViewById(R.id.btnCinco);
        btnSeis = (Button) findViewById(R.id.btnSeis);
        btnTres = (Button) findViewById(R.id.btnTres);
        btnDois = (Button) findViewById(R.id.btnDois);
        btnUm = (Button) findViewById(R.id.btnUm);
        btnPonto = (Button) findViewById(R.id.btnPonto);
        btnZero = (Button) findViewById(R.id.btnZero);
        btnCe = (Button) findViewById(R.id.btnCe);
        btnSoma = (Button) findViewById(R.id.btnSoma);
        btnSubtracao = (Button) findViewById(R.id.btnSubtracao);
        btnMultiplicacao = (Button) findViewById(R.id.btnMultiplicacao);
        btnDivisao = (Button) findViewById(R.id.btnDivisao);
        btnIgual = (Button) findViewById(R.id.btnIgual);
        display = (TextView) findViewById(R.id.display);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        limpar = false;

        initComponentes();

        btnZero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btnUm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btnDois.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btnTres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btnQuatro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btnCinco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btnSeis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btnSete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btnOito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });


        btnNove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });
        btnSoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcular();
            }
        });
        btnSubtracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcular();
            }
        });
        btnMultiplicacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcular();
            }
        });
        btnDivisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcular();
            }
        });
        btnCe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limparDisplay();
            }
        });
        btnPonto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });
    }


    private void calcular() {

        String numero = display.getText().toString();

        if (operando1 == 0) {
            operando1 = Double.parseDouble(numero);
            Log.i("#CALCULADORA", "Numero1:" + operando1);
        } else {
            operando2 = Double.parseDouble(numero);
            Log.i("#CALCULADORA", "Numero2:" + operando2);
        }
        limpar = true;

        if(operando1 != 0 && operando2 != 0) {
            if(operando2!=0) {
                operando1 = operando1 + operando2;
            }
            numero = String.valueOf(operando1);
            display.setText(numero);
            operando2 = 0;
        }


    }
    private void limparDisplay() {
        this.display.setText(ZERO);
        this.operando1 = 0;
        this.operando2 = 0;
    }




    private void escreverDisplay(View view) {

                    Button button = (Button) view;
                    String texto = display.getText().toString();
                    String valor = button.getText().toString();
                    if(texto.equals("0")){
                        if(valor.equals(".")){
                            texto = "0" + button.getText();
                            display.setText(texto);
                        } else{
                            display.setText(valor);;
                        }
                    }else{
                        if(texto.contains(".")){
                            if(!valor.equals(".")){
                                texto += button.getText();
                                display.setText(texto);
                            }
                        }else{
                            texto += button.getText();
                            display.setText(texto);
                        }
                    }










}
}